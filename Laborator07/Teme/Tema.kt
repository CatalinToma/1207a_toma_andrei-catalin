//am luat history.log de la un coleg. din masina mea virtuala history.log e un fisier complet gol

import java.io.File
import java.sql.Timestamp


class HistoryLogRecord(private val startDate: Timestamp, private val commandLine: String): Comparable<Timestamp>{
    fun print(){
        println("[$startDate] $commandLine\n")
    }
    fun getDate(): Timestamp{
        return startDate
    }
    fun getCommand(): String{
        return commandLine
    }
    override fun compareTo(other: Timestamp): Int {
        return startDate.toString().compareTo(other.toString())
    }
}

fun main(args: Array<String>) {
    val file = File("history.log")
    val lines:List<String> = file.readLines()
    val logs = mutableListOf<HistoryLogRecord>()
    var i = 0
    val _hashmap: HashMap<Timestamp, HistoryLogRecord> = HashMap<Timestamp, HistoryLogRecord>()
    while(i < lines.size - 4)
    {
        val sd = lines[i].substring(12)
        val aux = sd.substring(0, 10) + " " + sd.substring(12, 20)
        val ts: Timestamp = Timestamp.valueOf(aux)
        i += 1
        val cl = lines[i].substring(13)
        logs.add(HistoryLogRecord(ts, cl))
        //_hashmap.put(ts, HistoryLogRecord(ts, cl))
        if(logs.size == 51){
            logs.removeFirst()
        }
        i += 1
        while(!lines[i].contains("Start-Date: ") && (i + 1) < lines.size){
            i += 1
        }
    }

    for(log in logs)
    {
        _hashmap.put(log.getDate(), log) //prefer asa pentru ca nu stiu cum sa fac sa limitez
                                        //nr de elemente din hasmap la 50, cum se cere in enunt
    }

    for(key in _hashmap.keys){
        println("Key: $key:")
        print("Element: ")
        _hashmap[key]?.print()
    }
}