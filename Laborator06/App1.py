#EXEMPLU AGENDA

class ContactList(list):
    def search(self, name):
        matching_contacts = []
        for contact in self:
            if name in contact.name:
                matching_contacts.append(contact)
        return matching_contacts

class Contact:
    def __init__(self, name, email): # constructor
        self.name = name
        self.email = email
    # echivalentul supraincarcarii operator<< din c++
    def __repr__(self):
        return "Contact({}, {})".format(self.name, self.email)


class Friend(Contact):
    def __init__(self, name, email, phone):
        super().__init__(name, email)
        self.phone = phone

class Agenda:
    all_contacts = ContactList()

    def add_contact(self, contact):
        self.all_contacts.append(contact)

    def print_agenda(self):
        for contact in self.all_contacts:
            print(contact)

    def search(self, contact):
        for c in self.all_contacts:
            if c.name == contact.name and c.email == contact.email:
                return True
        return False

if __name__ == '__main__':
    agenda = Agenda()
    agenda.add_contact(Contact('Ion Popescu','ion_popescu@gmail.com'))
    agenda.add_contact(Contact('George Ionescu', 'g_ionescu@gmail.com'))
    agenda.add_contact(Friend('Stefan CelMare', 'fanel_rupe_turcii@gmail.com', '0700112233'))
    agenda.print_agenda()
    cauta = Friend('Stefan CelMare', 'fanel_rupe_turcii@gmail.com', '0700112233')
    print(agenda.search(cauta))