import threading
import multiprocessing
from concurrent.futures import ThreadPoolExecutor
import time
import random

#masuratori cu sortare: GIl: 0.3437   secvential: 0.3490   paralel cu multiproc: 1.2328   paralel cu futures: 0.3395
#masuratori filtrare: GIl: 4.1271   secvential: 4.1961   paralel cu multiproc: 3.3004   paralel cu futures: 4.2727
#masuratori sortare/filtrare: GIl: 2.3967   secvential: 2.3294   paralel cu multiproc: 3.2939   paralel cu futures: 2.3819
#masuratori incrementare: GIl: 0.0628   secvential: 0.0568   paralel cu multiproc: 1.1304   paralel cu futures: 0.0558

number_list = []
for i in range(0, 1000000):
    n = random.randint(1,1024)
    number_list.append(n)


def zeros_list(x):
    zeros_list = []
    for i in range(0, x):
        zeros_list.append(0)
    return zeros_list


def isPrime(nr):
    for i in range(2, int(nr/2)):
        if nr % i == 0:
            return False
    return True


def sortare():
    current_list = number_list.copy()
    current_list.sort()


def incrementare(current_list):
    for nr in current_list:
        nr += 1


def filtrare():
    current_list = []
    for nr in number_list:
        if isPrime(nr):
            current_list.append(nr)


def ver_1(list):
    thread_1 = threading.Thread(target=incrementare(list))
    thread_2 = threading.Thread(target=incrementare(list))
    thread_1.start()
    thread_2.start()
    thread_1.join()
    thread_2.join()


def ver_2(list):
    incrementare(list)
    incrementare(list)


def ver_3(list):
    process_1 = multiprocessing.Process(target=incrementare(list))
    process_2 = multiprocessing.Process(target=incrementare(list))
    process_1.start()
    process_2.start()
    process_1.join()
    process_2.join()


def ver_4(list):
    with ThreadPoolExecutor(max_workers=2) as executor:
        future = executor.submit(incrementare(list))
        future = executor.submit(incrementare(list))


if __name__ == '__main__':
    list = zeros_list(1000000)
    start = time.time()
    ver_1(list)
    end = time.time()
    print("\n Timp executie pseudoparalelism cu GIL")
    print(end - start)

    list = zeros_list(1000000)
    start = time.time()
    ver_2(list)
    end = time.time()
    print("\n Timp executie secvential")
    print(end - start)

    list = zeros_list(1000000)
    start = time.time()
    ver_3(list)
    end = time.time()
    print("\n Timp executie paralela cu multiprocessing")
    print(end - start)

    list = zeros_list(1000000)
    start = time.time()
    ver_4(list)
    end = time.time()
    print("\n Timp executie paralela cu concurrent.futures")
    print(end - start)
