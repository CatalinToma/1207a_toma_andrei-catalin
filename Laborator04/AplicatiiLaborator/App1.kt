class Content(aauth: String, ttext: String, nname: String, ppubl: String)
{
    private var author: String = aauth
    private var text: String = ttext
    private var name: String = nname
    private var publisher: String = ppubl

    fun getAuthor(): String
    {
        return author
    }
    fun setAuthor(new_author: String)
    {
        author = new_author
    }
    fun getText(): String
    {
        return text
    }
    fun setText(new_text: String)
    {
        text = new_text
    }
    fun getName(): String
    {
        return name
    }
    fun setName(new_name: String)
    {
        name = new_name
    }
    fun getPublisher(): String
    {
        return publisher
    }
    fun setPublisher(new_publisher: String)
    {
        publisher = new_publisher
    }
}

class Book(ddata: Content)
{
    private var data: Content = ddata

    fun getName(): String
    {
        return data.getName()
    }
    fun getAuthor(): String
    {
        return data.getAuthor()
    }
    fun getPublisher(): String
    {
        return data.getPublisher()
    }
    fun getContent(): String
    {
        return data.getText()
    }
    fun hasAuthor(_author: String): Boolean
    {
        return data.getAuthor() == _author
    }
    fun hasTitle(_title: String): Boolean
    {
        return data.getName() == _title
    }
    fun isPublishedBy(_publ: String): Boolean
    {
        return data.getPublisher() == _publ
    }

}

class Library(bbooks: MutableSet<Book>)
{
    private var books: MutableSet<Book> = bbooks

    fun getBooks(): MutableSet<Book> {
        return books
    }
    fun addBook(new_book: Book)
    {
        books.add(new_book)
    }
    fun findAllByAuthor(_author: String): MutableSet<Book>
    {
        lateinit var books_by: MutableSet<Book>
        books.forEach{
            if(it.getAuthor() == _author)
                books_by.add(it)
        }
        return books_by
    }
    fun findAllByName(_name: String): MutableSet<Book>
    {
        lateinit var books_by: MutableSet<Book>
        books.forEach{
            if(it.getName() == _name)
                books_by.add(it)
        }
        return books_by
    }
    fun findAllByPublisher(_publ: String): MutableSet<Book>
    {
        lateinit var books_by: MutableSet<Book>
        books.forEach{
            if(it.getPublisher() == _publ)
                books_by.add(it)
        }
        return books_by
    }

}

interface LibraryPrinter {
    public fun print(books : Set<Book>)
}

object printRAW : LibraryPrinter{
    override public fun print(books : Set<Book>){
        for(book in books){
            println("Titlu:" + book.getName())
            println("Autor:" + book.getAuthor())
            println("Publicat de:" + book.getPublisher())
            println("Continut: " + book.getContent())
        }
    }
}

fun main(){

}