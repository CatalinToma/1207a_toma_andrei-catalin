import copy
from abc import ABC, abstractmethod


class File(ABC):
    title = ""
    author = ""
    paragraphs = []

    def read_file_from_stdin(self):
        self.title = input("Titlu: ")
        self.author = input("Autor: ")
        nr_paragraphs = int(input("Numar de paragrafe: "))
        for i in range(1, nr_paragraphs + 1):
            paragraph = input("Paragraful " + str(i) + ": ")
            self.paragraphs.append(paragraph)


class HTMLFile(File):
    def __init__(self, title, author, paragraphs):
        self.title = title
        self.author = author
        self.paragraphs = paragraphs

    def print_html(self):
        print("<html>")

        print('\t' + "<head>")
        print("\t\t" + "<title>" + self.title + "</title>")
        print('\t' + "</head>")

        print('\t' + "<body>")
        print("\t\t" + "<h1>" + self.title + "</h1>")
        print("\t\t" + "<h2>" + "Autor: " + self.author + "</h2>")

        for paragraph in self.paragraphs:
            print("\t\t" + "<p>" + paragraph + "</p>")

        print('\t' + "</body>")

        print("</html>")


class JSONFile(File):
    def __init__(self, title, author, paragraphs):
        self.title = title
        self.author = author
        self.paragraphs = paragraphs

    def print_json(self):
        print("{")
        print("\t\"title\":  " + self.title)
        print("\t\"author\":  " + self.author)
        print("\t\"paragraphs\": [")

        for i in range(len(self.paragraphs) - 1):
            print("\t\t" + "\"" + self.paragraphs[i] + "\",")
        print("\t\t" + "\"" + self.paragraphs[len(self.paragraphs) - 1] + "\"")
        print("\t]")
        print("}")


class TextFile(File):
    template = ""

    def __init__(self, title, author, paragraphs):
        self.title = title
        self.author = author
        self.paragraphs = paragraphs

    def print_text(self):
        print("Template: " + self.template)
        print("Titlu: " + self.title)
        print("Autor: "+ self.author)
        print("Continut:")
        for paragraph in self.paragraphs:
            print(paragraph)

    def clone(self):
        return copy.copy(self)


class ArticleTextFile(TextFile):
    def __init__(self, title, author, paragraphs):
        super(ArticleTextFile, self).__init__(title, author, paragraphs)
        self.template = "Article"

    def print_text(self):
        print("\t\t" + self.title)
        print("\t\t\t\tby " + self.author)
        print()
        for paragraph in self.paragraphs:
            print(paragraph)


class BlogTextFile(TextFile):
    def __init__(self, title, author, paragraphs):
        super(BlogTextFile, self).__init__(title, author, paragraphs)
        self.template = "Blog"

    def print_text(self):
        print(self.title)
        for paragraph in self.paragraphs:
            print(paragraph)
        print()
        print("Written by " + self.author)


class FileFactory:
    def factory(self, file_type):
        new_file = File()
        new_file.read_file_from_stdin()
        if file_type == "HTML":
            return HTMLFile(new_file.title, new_file.author, new_file.paragraphs)
        if file_type == "JSON":
            return JSONFile(new_file.title, new_file.author, new_file.paragraphs)
        if file_type == "Article":
            return ArticleTextFile(new_file.title, new_file.author, new_file.paragraphs)
        if file_type == "Blog":
            return BlogTextFile(new_file.title, new_file.author, new_file.paragraphs)


if __name__ == '__main__':
    Factory = FileFactory()
    file1 = Factory.factory("Article")
    file1.print_text()
    file2 = file1.clone()
    file2.print_text()
    super(ArticleTextFile, file1).print_text()
