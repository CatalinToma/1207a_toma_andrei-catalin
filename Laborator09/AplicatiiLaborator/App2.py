import glob, os

def lineCounter(list):
    for file in list:
        txtfile = open(file[1] + '\\' + file[0])
        nr = 0
        for line in txtfile:
            if line != "\n":
                nr += 1
        txtfile.close()
        yield (file[0], nr)

def txtFilter(list):
    for file in list:
        if file[0].endswith(".txt"):
            yield file

def fileFilter(list):
    for dir in list:
        os.chdir(dir)
        for file in glob.glob("*.*"):
            yield (file, dir)


if __name__ == '__main__':
    dirlist = []
    path1 = "D:\Lab PP\Lab9\TextDir"
    path2 = "D:\Lab PP\Lab9\TextDir\Dir2"
    dirlist.append(path1)
    dirlist.append(path2)

    filteredlist = lineCounter(txtFilter(fileFilter(dirlist)))

    for elem in filteredlist:
        print(elem[0], ': ', elem[1])