class State():
    def __int__(self):
        pass


class InsertMoney:
    def __init__(self, state_machine):
        self.state_machine = state_machine

    def insert_10bani(self):
        self.state_machine.update_amount_of_money(0.1)

    def insert_50bani(self):
        self.state_machine.update_amount_of_money(0.5)

    def insert_1leu(self):
        self.state_machine.update_amount_of_money(1)

    def insert_5lei(self):
        self.state_machine.update_amount_of_money(5)

    def insert_10lei(self):
        self.state_machine.update_amount_of_money(10)


class WaitForClient():
    def client_arrive(self):
        arrived = input("Apasati enter: ")
        return 0


class TakeMoneySTM:
    def __init__(self):
        self.money = 0
        self.wait_state = WaitForClient()
        self.insert_money_state = InsertMoney(self)
        self.current_state = WaitForClient()
        self.wait_state.client_arrive()

    def add_money(self):
        print("\nAti introdus pana acum: " + str(self.money) + " lei")
        print("Format bani: 10bani    50bani    1leu    5lei    10lei")
        amount = input("Introduceti suma de bani sau 0 pentru a continua: ")
        if amount == "10bani":
            self.insert_money_state.insert_10bani()
            self.add_money()
        elif amount == "50bani":
            self.insert_money_state.insert_50bani()
            self.add_money()
        elif amount == "1leu":
            self.insert_money_state.insert_1leu()
            self.add_money()
        elif amount == "5lei":
            self.insert_money_state.insert_5lei()
            self.add_money()
        elif amount == "10lei":
            self.insert_money_state.insert_10lei()
            self.add_money()
        elif amount == "0":
            print("Ati introdus in total: " + str(self.money) + " lei")
        else:
            print("Ati introdus gresit. Reincercati:")
            self.add_money()

    def update_amount_of_money(self, value):
        self.money += value


class CocaCola:
    price = 3.5


class Pepsi:
    price = 3


class Sprite:
    price = 2.5

class SelectProduct:
    def choose(self):
        print("Produse:")
        print("   1. Coca-Cola 0.5l - 3.5 lei")
        print("   2. Pepsi 0.5l - 3 lei")
        print("   3. Sprite 0.5 - 2.5 lei")
        alegere = input("Introduceti numarul corespunzator produsului dorit: ")
        if alegere == "1":
            return CocaCola.price
        elif alegere == "2":
            return Pepsi.price
        elif alegere == "3":
            return Sprite.price
        else:
            print("Ati introdus gresit. Reincercati: ")
            self.choose()

class SelectProductSTM:
    def __init__(self):
        self.coca_cola_state = CocaCola()
        self.pepsi_state = Pepsi()
        self.sprite_state = Sprite()
        self.select_product_state = SelectProduct()

    def chose_another_product(self):
        return self.select_product_state.choose()


class VendingMachineSTM:
    def __init__(self):
        self.money_needed = 0
        self.money_introduced = 0
        self.take_money_stm = TakeMoneySTM()
        self.select_product_stm = SelectProductSTM()

    def mai_putini_bani(self):
        print("Ati introdus cu " + str(self.money_needed - self.money_introduced) + " lei mai putin decat costa produsul selectat.")
        alegere = input("Selectati 1 pentru a introduce diferenta sau 2 pentru a primi banii inapoi: ")
        if alegere == "1":
            self.take_money_stm.add_money()
        elif alegere == "2":
            rest = self.money_introduced
            self.take_money_stm.update_amount_of_money(float(-1 * rest))
            self.money_introduced = self.take_money_stm.money
            print("Ati primit inapoi " + str(rest) + " lei")
            print("Au ramas in aparat: " + str(self.money_introduced) + " lei")
        else:
            print("Ati introdus gresit. Reincercati:")
            self.mai_putini_bani()

    def mai_multi_bani(self):
        alegere = input("Introduceti 1 pentru a primi restul sau 2 pentru a cumpara alt produs: ")
        if alegere == "1":
            rest = self.money_introduced
            self.take_money_stm.update_amount_of_money(float(-1 * rest))
            self.money_introduced = self.take_money_stm.money
            print("Ati primit inapoi " + str(rest) + " lei")
            print("Au ramas in aparat: " + str(self.money_introduced) + " lei")
        elif alegere == "2":
            self.proceed_to_checkout()
        else:
            print("Ati introdus gresit. Reincercati: ")
            self.mai_multi_bani()

    def proceed_to_checkout(self):
        self.money_needed = self.select_product_stm.chose_another_product()
        self.money_introduced = self.take_money_stm.money
        print("Introduceti " + str(self.money_needed - self.money_introduced) + " lei pentru a primi produsul.")
        self.take_money_stm.add_money()
        self.money_introduced = self.take_money_stm.money

        if self.money_needed > self.money_introduced:
            self.mai_putini_bani()
        else:
            self.take_money_stm.update_amount_of_money(-1 * self.money_needed)
            print("Ati primit produsul.")
            self.money_introduced = self.take_money_stm.money
            print("Au ramas in aparat: " + str(self.money_introduced) + " lei")
            self.money_introduced = self.take_money_stm.money
            self.mai_multi_bani()

if __name__ == '__main__':
    vms = VendingMachineSTM()
    vms.proceed_to_checkout()