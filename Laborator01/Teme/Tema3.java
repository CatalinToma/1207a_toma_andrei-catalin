import java.io.*;
import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException
    {
	    File file = new File("fisier.txt");
	    Scanner myReader = new Scanner(file);
	    while(myReader.hasNextLine())
        {
            String linie = myReader.nextLine();
            //elimina semnele de punctuatie
            linie = linie.replace(".", "");
            linie = linie.replace(",", "");
            linie = linie.replace("!", "");
            linie = linie.replace("?", "");
            linie = linie.replace(";", "");
            linie = linie.replace(":", "");
            linie = linie.replace("\'", "");
            linie = linie.replace("\"", "");
            linie = linie.replace("(", "");
            linie = linie.replace(")", "");

            //lowercase to uppercase
            linie = linie.toUpperCase();

            //filtreaza numerele
            linie = linie.replaceAll("[0-9]", "");

            System.out.println(linie);
        }
    }
    
}
