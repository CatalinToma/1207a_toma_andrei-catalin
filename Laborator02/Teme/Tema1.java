//PROBLEMELE 1 SI 2

//import libraria principala polyglot din graalvm
import org.graalvm.polyglot.*;

import java.util.List;

//clasa principala - aplicatie JAVA
class Polyglot {
    //metoda privata pentru conversie low-case -> up-case folosind functia toupper() din R
    private static String RToUpper(String token){
        //construim un context care ne permite sa folosim elemente din R
        Context polyglot = Context.newBuilder().allowAllAccess(true).build();
        //folosim o variabila generica care va captura rezultatul excutiei funcitiei R, toupper(String)
        //pentru aexecuta instructiunea I din limbajul X, folosim functia graalvm polyglot.eval("X", "I");
        Value result = polyglot.eval("R", "toupper(\""+token+"\");");
        //utilizam metoda asString() din variabila incarcata cu output-ul executiei pentru a mapa valoarea generica la un String
        String resultString = result.asString();
        // inchidem contextul Polyglot
        polyglot.close();

        return resultString;
    }

    //metoda privata pentru evaluarea unei sume de control simple a literelor unui text ASCII, folosind PYTHON
    private static int SumCRC(String token){
        //construim un context care ne permite sa folosim elemente din PYTHON
        Context polyglot = Context.newBuilder().allowAllAccess(true).build();
        //folosim o variabila generica care va captura rezultatul excutiei functiei PYTHON, sum()
        //avem voie sa inlocuim anumite elemente din scriptul pe care il construim spre evaluare, aici token provine din JAVA, dar va fi interpretat de PYTHON
        Value result = polyglot.eval("python", "sum(ord(ch) for ch in '" + token + "')");
        //utilizam metoda asInt() din variabila incarcata cu output-ul executiei, pentru a mapa valoarea generica la un Int
        int resultInt = result.asInt();
        // inchidem contextul Polyglot
        polyglot.close();

        return resultInt;
    }
    public static int SumaPolinom(String token)
    {
        int crc = SumCRC(token);
        return crc*crc - crc;
    }

    //functia MAIN
    public static void main(String[] args) {
        //construim un context pentru evaluare elemente JS
        Context polyglot = Context.create();
        //construim un array de string-uri, folosind cuvinte din pagina web:  https://chrisseaton.com/truffleruby/tenthings/
        Value array = polyglot.eval("js", "[\"If\",\"we\",\"run\",\"the\",\"java\"];");
        //pentru fiecare cuvant, convertim la upcase folosind R si calculam suma de control folosind PYTHON

        long dim = array.getArraySize();
        int sumeCRC[] = new int[(int) dim];
        boolean afisat[] = new boolean[(int) dim];
        for (int i = 0; i < array.getArraySize();i++){
            String element = array.getArrayElement(i).asString();
            String upper = RToUpper(element);
            String nofirst_nolast = "";
            if(upper.length() > 2)
            {
                nofirst_nolast = upper.substring(1, upper.length() - 1);
            }

            int crc = SumCRC(nofirst_nolast);
            int sumP = SumaPolinom(nofirst_nolast);
            System.out.println(nofirst_nolast + " -> " + crc + " -> " + sumP);
            sumeCRC[i] = crc;
        }

        System.out.println();
        for (int i = 0; i < array.getArraySize();i++)
        {
            if(!afisat[i])
            {
                afisat[i] = true;
                String element = array.getArrayElement(i).asString();
                System.out.print("Elemente care au suma de control " + sumeCRC[i] + ": " + element);
                for(int j = i+1; j < array.getArraySize(); ++j)
                {
                    if(sumeCRC[i] == sumeCRC[j])
                    {
                        afisat[j] = true;
                        String element2 = array.getArrayElement(i).asString();
                        System.out.print(", " + element2);
                    }
                }
            }
            System.out.println();
        }


        // inchidem contextul Polyglot
        polyglot.close();
    }
}

