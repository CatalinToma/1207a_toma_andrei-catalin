package com.pp.laborator

import kotlinx.coroutines.*

fun log1(msg: String) = println("[${Thread.currentThread().name}] $msg")

fun main() = runBlocking<Unit> {
    log1("Started main coroutine")
    // run two background value computations
    val v1 = async(CoroutineName("v1coroutine")) {
        delay(500)
        log("Computing v1")
        256
    }
    val v2 = async(CoroutineName("v2coroutine")) {
        delay(1000)
        log("Computing v2")
        8
    }
    log1("The answer for v1 / v2 = ${v1.await() / v2.await()}")
}
