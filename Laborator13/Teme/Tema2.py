import more_itertools
from functional import seq
if __name__ == '__main__':
    string = "acesta este un text care are mai multe cuvinte"
    string = string.split(' ')
    mapped = seq(string).map(lambda x: (x[0], x))
    print(mapped)
    key_f = lambda x: x[0]
    reduced = more_itertools.map_reduce(string, key_f)
    print(reduced)
