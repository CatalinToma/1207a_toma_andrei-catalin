class customList(list):
    def removeLowerThan(self, minimum):
         for nr in self:
             if nr < minimum:
                self.remove(nr)
    def grupeaza(self):
         newList = customList([])
         for i in range(1, len(self), 2):
            newList.append((self[i-1], self[i]))
         return newList
    def multiplica(self):
        newList = customList([])
        for grup in self:
            newList.append(grup[0] * grup[1])
        return newList
    def sumaLista(self):
         sum = 0
         for nr in self:
            sum += nr
         return sum
    def listaProcesata(self):
         lista = self
         print(lista)
         lista.removeLowerThan(5)
         print(lista)
         lista = lista.grupeaza()
         print(lista)
         lista = lista.multiplica()
         print(lista)
         rez = lista.sumaLista()
         return rez
if __name__ == '__main__':
     myList = customList([1, 21, 75, 39, 7, 2, 35, 3, 31, 7, 8])
     print(myList.listaProcesata())
