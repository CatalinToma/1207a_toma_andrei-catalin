from functional import seq
if __name__ == '__main__':
     string = "aaaabbbccdeeff"
     string = [litera for litera in string]
     mapped = seq(string).map(lambda word: (word, 1))\
        .reduce_by_key(lambda x, y: x + y)
     newString = ""
     for it in mapped:
        newString += it[0]
     print(newString)
