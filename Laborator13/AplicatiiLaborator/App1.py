class customInt(int):
     def isPrime(self):
         for i in range(2, int(self / 2)):
            if self % i == 0:
                return False
         return True
if __name__ == '__main__':
     print(str(7) + " is prime?: " + str(customInt(7).isPrime()))
     print(str(13) + " is prime?: " + str(customInt(13).isPrime()))
     print(str(25) + " is prime?: " + str(customInt(25).isPrime()))