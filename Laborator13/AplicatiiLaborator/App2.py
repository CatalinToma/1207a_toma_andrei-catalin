class customString(str):
     def toPascalCase(self):
        return self.title().replace(" ", "")
if __name__ == '__main__':
     string = customString("Acesta este un text convertit la PascalCase")
     print(string.toPascalCase())