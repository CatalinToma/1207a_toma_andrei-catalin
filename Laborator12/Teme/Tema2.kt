import java.io.File
import java.io.InputStream

fun String.toCaesarCipher(key: Int): String{
    val offset = key % 26
    var cuvant = this.toString()

    for(i in this.indices) {
        var litera = this[i]
        if (litera in 'a'..'z') {
            litera += offset
            if (litera > 'z') {
                litera -= 26
            } else if (litera < 'a') {
                litera += 26
            }
        } else if (litera in 'A'..'Z') {
            litera += offset
            if (litera > 'Z') {
                litera -= 26
            } else if (litera < 'A') {
                litera += 26
            }
        }
        cuvant = cuvant.substring(0, i) + litera + cuvant.substring(i + 1)
    }
    return cuvant
}

fun String.ProceseazaCuvinte(key: Int): String {
    var cuvinte = this.split(" ")
    var codedString = ""

    for(cuvant in cuvinte){
        var newWord = cuvant
        if(cuvant.length in 4..7){
            newWord = cuvant.toCaesarCipher(key)
        }
        codedString += newWord + " "
    }

    return codedString
}

fun main() {
    val inputStream: InputStream = File("input.txt").inputStream()
    val textCodat = mutableListOf<String>()
    val offset = 3

    inputStream.bufferedReader().forEachLine {
        var aux = it.ProceseazaCuvinte(offset)
        textCodat.add(aux)
    }
    textCodat.forEach{println(it)}
}