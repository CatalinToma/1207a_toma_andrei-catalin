import org.omg.CORBA.Object

fun MutableList<Int>.scoateNrMaiMici(valMin: Int): MutableList<Int>{
    val newList = mutableListOf<Int>()
    for(i in this.indices){
        if(this[i] >= valMin){
            newList.add(this[i])
        }
        //println(newList[i])
    }
    return newList
}

fun MutableList<Int>.grupeaza(): MutableList<Pair<Int, Int>>{
    val newList = mutableListOf<Pair<Int,Int>>()
    for(i in 1 until this.size step 2){
        newList.add(Pair(this[i-1], this[i]))
    }
    return newList
}

fun MutableList<Pair<Int, Int>>.inmulteste(): MutableList<Int>{
    val newList = mutableListOf<Int>()
    for(pereche in this){
        newList.add(pereche.first * pereche.second)
    }
    return newList
}

fun MutableList<Int>.aduna(): Int{
    var sum = 0
    for(nr in this){
        sum += nr
    }
    return sum
}

fun MutableList<Int>.operatii() {
    println(this)
    val newList1 = this.scoateNrMaiMici(5)
    println(newList1)
    val newList2 = newList1.grupeaza()
    println(newList2)
    val newList3 = newList2.inmulteste()
    println(newList3)
    val sum = newList3.aduna()
    println(sum)
}


fun main(args: Array<String>) {
    val lista = mutableListOf<Int>(1, 21, 75, 39, 7, 2, 35, 3, 31, 7, 8)
    lista.operatii()
}