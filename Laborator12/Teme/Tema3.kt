import java.io.File
import java.io.InputStream
import java.lang.Math.sqrt
import java.util.*

fun Pair<Pair<Float, Float>, Pair<Float, Float>>.lungimeLinie(): Double{
    val x1 = this.first.first.toDouble()
    val y1 = this.first.second.toDouble()
    val x2 = this.second.first.toDouble()
    val y2 = this.second.second.toDouble()

    val lungime =  sqrt((x2-x1)*(x2-x1) + (y2 - y1) * (y2 - y1))

    //println("linia: ($x1, $y1), ($x2, $y2) are lungimea $lungime")
    return lungime
}

fun MutableList<Float>.Perimetru(): Double{
    val n = this[0].toInt()
    this.removeAt(0)
    //println("$n: $this")
    if(this.size != 2 * n){
        println("Date introduse gresit")
        return (-1).toDouble()
    }
    else{
        val puncte = mutableListOf<Pair<Float, Float>>()// = this.zipWithNext()
        for(i in 1 until this.size step 2){
            puncte.add(Pair(this[i-1], this[i]))
        }
        //var linii = mutableListOf<Pair<Pair<Float, Float>, Pair<Float, Float>>>()
        val linii = puncte.zipWithNext() as MutableList<Pair<Pair<Float, Float>, Pair<Float, Float>>>
        linii.add(Pair(puncte[puncte.size-1], puncte[0]))
        var perimetru = 0.toDouble()
        linii.forEach{
            perimetru += it.lungimeLinie()
        }
        //println(perimetru)
        return perimetru
    }
}

fun main(args: Array<String>) {
    val inputStream: InputStream = File("input.txt").inputStream()
    val poligon = mutableListOf<Float>()
    inputStream.bufferedReader().forEachLine {
        val numbers = it.split(" ")
        numbers.forEach{
            poligon.add(it.toFloat())
        }
    }
    print("Poligonul dat are periimetrul egal cu ${poligon.Perimetru()}")
}